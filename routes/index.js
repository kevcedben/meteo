var http = require('http');
var express = require('express');
var router = express.Router();

var apiPrefix = 'http://api.openweathermap.org/data/2.5';
var apiSuffix = '&units=metric&APPID=7958d95b0baa5af57b56de59b727aab7';
var countryCode = 'fr';

var apiRequest = function(city) {
  var callback = () => {};
  var _return = new Object({then: (cb) => {
    callback = cb;
  }});

  http.get(apiPrefix + '/weather?q=' + city + ',' + countryCode + apiSuffix, (result) => {
    var { statusCode } = result;
    var contentType = result.headers['content-type'];

    let error;
    if (statusCode !== 200) {
      error = new Error('Request Failed.\n' +
                        `Status Code: ${statusCode}`);
    } else if (!/^application\/json/.test(contentType)) {
      error = new Error('Invalid content-type.\n' +
                        `Expected application/json but received ${contentType}`);
    }
    if (error) {
      console.error(error.message);
      // consume response data to free up memory
      result.resume();
      return _return;
    }

    result.setEncoding('utf8');
    let rawData = '';
    result.on('data', (chunk) => { rawData += chunk; });
    result.on('end', () => {
      try {
        callback(JSON.parse(rawData));
      } catch (e) {
        console.error(e.message);
      }
    });
  }).on('error', (e) => {
    console.error(`Got error: ${e.message}`);
  });

  return _return;
}

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.all('/*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  next();
});

router.get('/meteo/doc', function(req, res, next) {
  res.sendfile('doc.html');
});

router.get('/meteo/all/:city', function(req, res, next) {
  apiRequest(req.params.city).then((data) => {
    res.json(data);
  });
});

router.get('/meteo/temperature/:city', function(req, res, next) {
  apiRequest(req.params.city).then((data) => {
    res.json(data.main.temp);
  });
});

router.get('/meteo/temperature/min/:city', function(req, res, next) {
  apiRequest(req.params.city).then((data) => {
    res.json(data.main.temp_min);
  });
});

router.get('/meteo/temperature/max/:city', function(req, res, next) {
  apiRequest(req.params.city).then((data) => {
    res.json(data.main.temp_max);
  });
});

router.get('/meteo/weather/:city', function(req, res, next) {
  apiRequest(req.params.city).then((data) => {
    res.json(data.weather[0].main);
  });
});

router.get('/meteo/pressure/:city', function(req, res, next) {
  apiRequest(req.params.city).then((data) => {
    res.json(data.main.pressure);
  });
});

router.get('/meteo/humidity/:city', function(req, res, next) {
  apiRequest(req.params.city).then((data) => {
    res.json(data.main.humidity);
  });
});

module.exports = router;
